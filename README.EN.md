<center>

# `requests` for CangjieLang

[简体中文](https://www.bilibili.com/video/BV1GJ411x7h7) | [English](https://www.bilibili.com/video/BV1GJ411x7h7)

> ~~A Cangjie lib to send web requests.~~

</center>

---
> There's still some problems with v1.2.0, plz use v1.1.0 instead.
## Usage
Import the package first.
~~~cangjie
import requests.*
~~~
#### 0. Response
Define:
~~~cangjie
public class Response <: ToString {
    init(url: String, httpResponse: HttpResponse)
    public func raiseForStatus(): Unit
    public func toString(): String
    public let content: Array<Byte>
    public let headers: HttpHeaders
    public func json(): JsonObject
    public let reason: String
    public let status_code: UInt16
    public let text: String
    public let url: String
}
~~~
You can use it as `requests` in `Python`
#### 1. `GET`
~~~cangjie
public static func get<T>(
    url: String, 
    headers!: Array<(String, T)> = [], 
    params!: Array<(String, T)> = [], 
    timeout!: Int = 5
): Response where T <: ToString
~~~
> For example:
> ~~~cangjie
> let response = Requests.get("https://www.baidu.com/s", headers: [
>     (HttpHeader.USER_AGENT, "Chrome/114.5.1.4")
> ], params: [
>     ("wd", "田所浩二")
> ])
> response.raiseForStatus()
> println(response.text)
> ~~~
#### 2. `POST`
~~~cangjie
public static func post<T>(
    url: String,
    headers!: Array<(String, T)> = [],
    params!: Array<(String, T)> = [], 
    data!: Array<(String, T)> = [],
    json!: JsonObject = JsonObject(),
    timeout!: Int = 5
): Response where T <: ToString
~~~
#### 3. `OPTIONS`, `HEAD`, `PUT`, `PATCH`, `DELETE`
---

> Author：[MournInk](https://space.bilibili.com/598656355)
> 
> References：[@psf's requests](https://github.com/psf/requests)