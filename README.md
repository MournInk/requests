<center>

# `requests` for 仓颉

[简体中文](https://www.bilibili.com/video/BV1GJ411x7h7) | [English](https://www.bilibili.com/video/BV1GJ411x7h7)

> ~~一个因为仓颉的网络请求太繁琐而诞生的库~~

</center>

---
> v1.2.0 遇到了一些问题，请暂时使用 v1.1.0
## 使用方法
导入先
~~~cangjie
import requests.*
~~~
#### 0. Response
该类定义为
~~~cangjie
public class Response <: ToString {
    init(url: String, httpResponse: HttpResponse)
    public func raiseForStatus(): Unit
    public func toString(): String
    public let content: Array<Byte>
    public let headers: HttpHeaders
    public func json(): JsonObject
    public let reason: String
    public let status_code: UInt16
    public let text: String
    public let url: String
}
~~~
操作方法基本与 `Python` 的 `requests` 库相同
#### 1. `GET`
函数定义为：
~~~cangjie
public static func get<T>(
    url: String, 
    headers!: Array<(String, T)> = [], 
    params!: Array<(String, T)> = [], 
    timeout!: Int = 5
): Response where T <: ToString
~~~
> 例：
> ~~~cangjie
> let response = Requests.get("https://www.baidu.com/s", headers: [
>     (HttpHeader.USER_AGENT, "Chrome/114.5.1.4")
> ], params: [
>     ("wd", "田所浩二")
> ])
> response.raiseForStatus()
> println(response.text)
> ~~~
#### 2. `POST`
函数定义为：
~~~cangjie
public static func post<T>(
    url: String,
    headers!: Array<(String, T)> = [],
    params!: Array<(String, T)> = [], 
    data!: Array<(String, T)> = [],
    json!: JsonObject = JsonObject(),
    timeout!: Int = 5
): Response where T <: ToString
~~~
其中 `data` 对应的是 `application/x-www-form-urlencoded` 格式的数据，而 `json` 对应的是 `applitaion/json` 格式的数据。

需要注意的是，此处 `data` 与 `json` 不能同时传入，否则会抛出 `InvalidRequest` 异常。
#### 3. `OPTIONS`, `HEAD`, `PUT`, `PATCH`, `DELETE`
其定义与 `GET`、`POST` 类似

更具体的，`DELETE`、`PATCH`、`PUT` 具有与 `POST` 相同的定义

`OPTIONS`、`HEAD` 与 `GET` 类似，但没有 `params` 参数

---

欢迎各位大佬提 issue 或者 pr！

> 作者：[MournInk](https://space.bilibili.com/598656355)
> 
> 感谢：[@psf 的 requests](https://github.com/psf/requests)